<?php


Route::get('/shorten', function () {
    return view('home');
});

Route::get('/', function () {
    return redirect('/shorten');
});


Route::get('/{uri}', 'RedirectController@redirect');

Route::post('/', 'ShortenerController@store')->name('generate-short-url');

Route::get('/stats/{uri}', 'ShortenerController@stats');