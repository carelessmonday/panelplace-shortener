<?php

namespace App\Http\Controllers;

use App\Helpers\RandomString;
use App\Url;
use Illuminate\Http\Request;

class ShortenerController extends Controller {

    public function store(Request $request)
    {

        $validatedData = $request->validate([
            'url' => 'required|url',
        ]);

        $url = Url::where('original', $validatedData['url'])->first();

        if ($url) return view('shortened', compact('url'));

        $url = new Url;
        $url->original = $validatedData['url'];
        $url->uri = RandomString::generate('6');
        $url->save();

        return view('shortened', compact('url'));
    }

    public function stats($uri)
    {
        $url = Url::where('uri', $uri)->with('visitors')->first();
        if (!$url) return response('Nothing to do here.', 404);

        return $url;
    }

}
