<?php

namespace App\Http\Controllers;

use App\Url;
use App\Visitor;
use Illuminate\Http\Request;
use GeoIp2\Database\Reader;
use Exception;

class RedirectController extends Controller {

    public function redirect($uri, Request $request)
    {
        $url = Url::where('uri', $uri)->first();

        if (!$url) return redirect('/shorten');

        // Get Location
        $locationReader = new Reader(database_path('maxmind/GeoLite2-City.mmdb'));
        try {
            $locationRecord = $locationReader->city($this->getIpAddress());
            $visitorData = [
                "referer"    => ($request->server('HTTP_REFERER')) ? $request->server('HTTP_REFERER') : "/",
                "user_agent" => $request->server('HTTP_USER_AGENT'),
                "ip_address" => $this->getIpAddress(),
                "country"    => $locationRecord->country->name,
                "city"       => $locationRecord->city->name,
                "longitude"  => $locationRecord->location->longitude,
                "latidude"   => $locationRecord->location->latitude,
            ];
            $url->visitors()->create($visitorData);
        } catch (Exception $e) {
            // TODO

            $url->visitors()->create([
                "referer"    => ($request->server('HTTP_REFERER')) ? $request->server('HTTP_REFERER') : "/",
                "user_agent" => $request->server('HTTP_USER_AGENT'),
                "ip_address" => $this->getIpAddress(),
            ]);
        }


        return redirect($url->original);
    }

    // Work around to get user ip on heroku
    private function getIpAddress()
    {
        if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ipAddresses = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);

            return trim(end($ipAddresses));
        } else {
            return $_SERVER['REMOTE_ADDR'];
        }
    }

}
