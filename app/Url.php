<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Url extends Model
{
    protected $fillable = ['original', 'uri'];
    protected $hidden = ['id', 'updated_at'];

    public function visitors()
    {
        return $this->hasMany(Visitor::class);
    }

}
