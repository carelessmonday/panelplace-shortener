<?php

namespace App\Helpers;

use DB;

class RandomString {

    public static function generate($chars = 4)
    {
        $unique = FALSE;
        $tested = [];

        do {
            $random = str_random($chars);

            if (in_array($random, $tested)) continue;

            $count = DB::table('urls')->where('uri', '=', $random)->count();
            $tested[] = $random;

            if ($count == 0) $unique = TRUE;
        } while (!$unique);


        return $random;
    }

}
