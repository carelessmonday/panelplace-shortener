<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Visitor extends Model
{
    protected $fillable = ['referer', 'user_agent', 'ip_address', 'country', 'city', 'longitude', 'latitude'];
    protected $hidden = ['id', 'updated_at', 'url_id'];

    public function url()
    {
        return $this->belongsTo(Url::class);
    }
}
